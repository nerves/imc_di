﻿
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Acr.UserDialogs;
using TaxJarAPI_POC.Core;
using TaxJarAPI_POC.Core.Interfaces;
using TaxJarAPI_POC.Infrastructure;
using TaxJarAPI_POC.Utility;
using Xamarin.Forms;

namespace TaxJarAPI_POC.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        #region Fields

        private const string ValidationRequired = "REQUIRED";
        private const string ValidationSatisfied = "DONE";

        private readonly ITaxService _taxService;

        #endregion
        #region Properties

        public bool IsSaveButtonEnabled { get; set; }
        public string FromState { get; set; }
        public string FromStateValidation { get; set; }
        public string State { get; set; }
        public string StateValidation { get; set; }
        public string Country { get; set; }
        public string CountryValidation { get; set; }
        public string Zip { get; set; }
        public string ZipValidation { get; set; }
        public string Amount { get; set; }
        public string AmountValidation { get; set; }
        public string ShippingAmount { get; set; }
        public string ShippingAmountValidation { get; set; }
        public string CalcualtedTax { get; set; }
        public string TaxRate { get; set; }
        public string GrandTotal { get; set; }
        public string TaxableTotal { get; set; }
        public string SubTotal { get; set; }
        public List<string> CountryPickerItems { get; set; }
        public List<string> StatePickerItems { get; set; }
        public Command CalcualteTaxCommand { get; set; }

        #endregion
        #region Lifecycle

        public MainPageViewModel(ITaxService taxService)
        {
            _taxService = taxService;

            CalcualteTaxCommand = new Command(async () => { await CalcualeteTax(); });

            FromStateValidation = StateValidation = CountryValidation = ZipValidation = AmountValidation = ShippingAmountValidation = ValidationRequired;

            IsSaveButtonEnabled = true;

            CountryPickerItems = new List<string> { "US" };

            StatePickerItems = PopulateStates();

            SubTotal = 0d.ToCurrency();
            TaxRate = 0d.ToPercent();
            TaxableTotal = 0d.ToCurrency();
            GrandTotal = 0d.ToCurrency();
            CalcualtedTax = 0d.ToCurrency();
        }

        public override async Task<bool> OnNavigatedBack()
        {
            return false;
        }

        public override async Task Prepare()
        {
            ValidateInput();
        }

        public override async Task Refresh()
        {

        }

        public override async Task Start()
        {
            ValidateInput();
        }

        public override async Task Stop()
        {

        }

        #endregion
        #region Property Changed

        public void OnStateChanged()
        {
            ValidateInput();
        }

        public void OnCountryChanged()
        {
            ValidateInput();
        }

        public void OnZipChanged()
        {
            ValidateInput();
        }

        public void OnAmountChanged()
        {
            ValidateInput();
        }

        public void OnShippingAmountChanged()
        {
            ValidateInput();
        }

        #endregion

        public void ValidateInput()
        {
            // validate the UI input
            FromStateValidation = string.IsNullOrEmpty(FromState) ? ValidationRequired : ValidationSatisfied;
            StateValidation = string.IsNullOrEmpty(State) ? ValidationRequired : ValidationSatisfied;
            CountryValidation = string.IsNullOrEmpty(Country) ? ValidationRequired : ValidationSatisfied;
            ZipValidation = string.IsNullOrEmpty(Zip) ? ValidationRequired : ValidationSatisfied;
            AmountValidation = string.IsNullOrEmpty(Amount) || IsDouble(Amount) == false ? ValidationRequired : ValidationSatisfied;
            ShippingAmountValidation = string.IsNullOrEmpty(ShippingAmount) || IsDouble(ShippingAmount) == false ? ValidationRequired : ValidationSatisfied;

            // set calculate button state
            IsSaveButtonEnabled =
                StateValidation == ValidationSatisfied
                && FromStateValidation == FromStateValidation
                && CountryValidation == ValidationSatisfied
                && ZipValidation == ValidationSatisfied
                && AmountValidation == ValidationSatisfied
                && ShippingAmountValidation == ValidationSatisfied;

            // calculate subtotal
            if (double.TryParse(Amount, out double amount) && double.TryParse(ShippingAmount, out double shippingAmount))
            {
                SubTotal = (amount + shippingAmount).ToCurrency();
            }
            else
            {
                SubTotal = 0d.ToCurrency();
            }
        }

        private bool IsDouble(string s)
        {
            return double.TryParse(s, out _);
        }

        private async Task CalcualeteTax()
        {
            ShowLoading();
            if (double.TryParse(Amount, out double amount) && double.TryParse(ShippingAmount, out double shippingAmount))
            {
                var fromState = FromState.Split('-')[1].Trim();
                var state = State.Split('-')[1].Trim();
                var (responseModel, exceptionMessage) = await _taxService.CalculateTax<TaxJarTaxCalculator>(amount, shippingAmount, Country, Zip, state, fromState);

                if (string.IsNullOrEmpty(exceptionMessage) == false)
                {
                    HideLoading();
                    UserDialogs.Instance.Alert(exceptionMessage);
                    return;
                }

                GrandTotal = (responseModel?.Tax?.OrderTotalAmount + responseModel?.Tax?.AmountToCollect).ToCurrency();
                TaxableTotal = responseModel?.Tax?.TaxableAmount.ToCurrency();
                TaxRate = responseModel?.Tax?.Rate.ToPercent();
                CalcualtedTax = responseModel?.Tax?.AmountToCollect.ToCurrency();
                HideLoading();
            }
        }

        private static List<string> PopulateStates()
        {
            var assembly = typeof(MainPageViewModel).GetTypeInfo().Assembly;
            var name = typeof(MainPageViewModel).Assembly.GetName().Name;
            Stream stream = assembly.GetManifestResourceStream($"{name}.states.txt");
            string text = string.Empty;
            var words = new List<string>();
            using (var reader = new StreamReader(stream))
            {
                while ((text = reader.ReadLine()) != null)
                {
                    words.Add(text);
                }
            }

            return words;
        }
    }
}
