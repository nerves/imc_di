﻿using System;
using TaxJarAPI_POC.DependencyInjector;
using TaxJarAPI_POC.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TaxJarAPI_POC
{
    public partial class App : Application
    {
        public static IDependencyContainer IoC { get; private set; }

        public App()
        {
            InitializeComponent();

            // init the DI container
            IoC = new DependencyContainer();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
