﻿using System;
namespace TaxJarAPI_POC.Utility
{
    public static class ExtentionMethods
    {
        public static string ToCurrency(this double d)
        {
            return string.Format("{0:C}", d);
        }

        public static string ToCurrency(this double? d)
        {
            return string.Format("{0:C}", d);
        }

        public static string ToCurrency(this string s)
        {
            return string.Format("{0:C}", s);
        }

        public static string ToPercent(this string s)
        {
            return $"{s}%";
        }

        public static string ToPercent(this double d)
        {
            return $"{d}%";
        }
    }
}
