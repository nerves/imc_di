﻿
using System;
using System.Linq;
using SimpleInjector;
using TaxJarAPI_POC.Core.Interfaces;
using TaxJarAPI_POC.Infrastructure;
using TaxJarAPI_POC.Services;

namespace TaxJarAPI_POC.DependencyInjector
{
    internal class DependencyContainer : IDependencyContainer
    {
        private Container _container;

        public DependencyContainer()
        {
            _container = new Container();
            RegisterDependencies();

            try
            {
                _container.Verify();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(" RegisterDependencies Error " + e.Message);
            }
        }

        private void RegisterDependencies()
        {
            //Register singletons
            _container.Register<IDependencyContainer>(() => this, Lifestyle.Singleton);

            //Register transients
            _container.Register<ITaxService, TaxService>();

            //Register collections
            _container.Collection.Register<ITaxCalculator>(new TaxJarTaxCalculator(), new SomeOtherTaxCalculator());

            var assembly = typeof(DependencyContainer).Assembly;
            assembly.GetTypes()
                .Where(type => type.IsClass && !type.IsAbstract)
                .Where(type => typeof(IViewModel).IsAssignableFrom(type))
                .ToList()
                .ForEach(type => _container.Register(type, type, Lifestyle.Transient));
        }

        public object GetInstance(Type type) => _container.GetInstance(type);
        public TService GetInstance<TService>() where TService : class => _container.GetInstance<TService>();
    }
}
