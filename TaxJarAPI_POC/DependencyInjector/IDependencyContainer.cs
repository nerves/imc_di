﻿using System;
namespace TaxJarAPI_POC.DependencyInjector
{
    public interface IDependencyContainer
    {
        object GetInstance(Type type);
        TService GetInstance<TService>() where TService : class;
    }
}
