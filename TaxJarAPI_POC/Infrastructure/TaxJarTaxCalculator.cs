﻿
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TaxJarAPI_POC.Core.Interfaces;
using TaxJarAPI_POC.Core.WebModels;

namespace TaxJarAPI_POC.Infrastructure
{
    public class TaxJarTaxCalculator : ITaxCalculator
    {
        public TaxJarTaxCalculator()
        {
        }

        /// <summary>
        /// Returns tax data from a web call to TaxJar or an exception string
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="shipping"></param>
        /// <param name="toCountry"></param>
        /// <param name="toZip"></param>
        /// <param name="toState"></param>
        /// <param name="fromState"></param>
        /// <returns>tax data, exception string</returns>
        public async Task<(TaxAmountResponseModel model, string exception)> CalcualateTax(double amount, double shipping, string toCountry, string toZip, string toState, string fromState)
        {
            var model = new TaxAmountDto
            {
                ToCountry = toCountry,
                ToZip = toZip,
                ToState = toState,
                Amount = amount,
                Shipping = shipping,
                FromState = fromState
            };

            return await CalcualateTaxFromWeb(model);
        }

        private async Task<(TaxAmountResponseModel model, string exception)> CalcualateTaxFromWeb(TaxAmountDto model)
        {
            using (var client = new HttpClient())
            {
                var uri = $"https://api.taxjar.com/v2/taxes";
                await ConfigureClient(client, uri);

                var stringPayload = JsonConvert.SerializeObject(model, GetJsonSettings());
                var postVariables = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                string response = "";
                try
                {

                    HttpResponseMessage responseMessage = await client.PostAsync(uri, postVariables).ConfigureAwait(false);
                    response = await responseMessage.Content.ReadAsStringAsync();

                    // bad path
                    if (response.Contains("error"))
                    {
                        var errorResult = JsonConvert.DeserializeObject<Dictionary<string, string>>(response, GetJsonSettings());
                        return (null, errorResult["detail"]);
                    }

                    // happy path
                    var result = JsonConvert.DeserializeObject<TaxAmountResponseModel>(response, GetJsonSettings());

                    return (result, string.Empty);
                }
                catch (Exception ex)
                {
                    return (null, ex.Message);
                }
            }
        }

        /// <summary>
        /// Returns tax rate data from a web call to TaxJar or an exception string
        /// </summary>
        /// <param name="zip"></param>
        /// <returns>tax rate, exception string</returns>
        public async Task<(RateResponseModel model, string exception)> GetTaxRateForLocation(string zip)
        {
            var result = await GetTaxRateForLocationWithWeb(zip);
            return result;
        }

        private async Task<(RateResponseModel model, string exception)> GetTaxRateForLocationWithWeb(string zip)
        {
            using (var client = new HttpClient())
            {
                var uri = $"https://api.taxjar.com/v2/rates/{zip}";
                await ConfigureClient(client, uri);

                string response = "";
                try
                {

                    HttpResponseMessage responseMessage = await client.GetAsync(uri).ConfigureAwait(false);
                    response = await responseMessage.Content.ReadAsStringAsync();

                    // bad path
                    if (response.Contains("error"))
                    {
                        var errorResult = JsonConvert.DeserializeObject<Dictionary<string, string>>(response, GetJsonSettings());
                        return (null, errorResult["detail"]);
                    }

                    // happy path
                    var result = JsonConvert.DeserializeObject<RateResponseModel>(response, GetJsonSettings());

                    return (result, string.Empty);
                }
                catch (Exception ex)
                {
                    return (null, ex.Message);
                }
            }
        }

        private async Task ConfigureClient(HttpClient client, string address)
        {
            try
            {
                client.BaseAddress = new Uri(address);
                client.Timeout = TimeSpan.FromSeconds(30);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Token token=\"5da2f821eee4035db4771edab942a4cc\"");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static JsonSerializerSettings GetJsonSettings()
        {
            return new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
        }
    }
}
