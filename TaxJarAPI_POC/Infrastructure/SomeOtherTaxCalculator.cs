﻿using System;
using System.Threading.Tasks;
using TaxJarAPI_POC.Core.Interfaces;
using TaxJarAPI_POC.Core.WebModels;

namespace TaxJarAPI_POC.Infrastructure
{
    public class SomeOtherTaxCalculator : ITaxCalculator
    {
        public SomeOtherTaxCalculator()
        {
        }

        public Task<(TaxAmountResponseModel model, string exception)> CalcualateTax(double amount, double shipping, string toCountry, string toZip, string toState, string fromState)
        {
            throw new NotImplementedException();
        }

        public Task<(RateResponseModel model, string exception)> GetTaxRateForLocation(string zip)
        {
            throw new NotImplementedException();
        }
    }
}
