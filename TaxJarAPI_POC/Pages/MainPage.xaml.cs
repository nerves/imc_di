﻿
using TaxJarAPI_POC.Core;
using TaxJarAPI_POC.ViewModels;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace TaxJarAPI_POC.Pages
{
    public partial class MainPage : BasePage
    {
        public MainPage()
        {
            InitializeComponent();

            On<iOS>().SetUseSafeArea(true);

            BindingContext = App.IoC.GetInstance(typeof(MainPageViewModel)) as MainPageViewModel;
        }
    }
}
