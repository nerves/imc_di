﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaxJarAPI_POC.Core.Interfaces;
using TaxJarAPI_POC.Core.WebModels;

namespace TaxJarAPI_POC.Services
{
    public class TaxService : ITaxService
    {
        private IEnumerable<ITaxCalculator> _taxCalculators;

        public TaxService(IEnumerable<ITaxCalculator> taxCalculators)
        {
            _taxCalculators = taxCalculators;
        }

        /// <summary>
        /// Calculates the tax due using the Tax Calculator of the type passed
        /// </summary>
        /// <typeparam name="T">Calculator type to use</typeparam>
        /// <param name="amount"></param>
        /// <param name="shipping"></param>
        /// <param name="toCountry"></param>
        /// <param name="toZip"></param>
        /// <param name="toState"></param>
        /// <param name="fromState"></param>
        /// <returns>TaxAmountResponseModel with data and exception string</returns>
        public async Task<(TaxAmountResponseModel responseModel, string exceptionMessage)> CalculateTax<T>(double amount, double shipping, string toCountry, string toZip, string toState, string fromState) where T : ITaxCalculator
        {
            return await Calculate(amount, shipping, toCountry, toZip, toState, fromState, typeof(T));
        }

        /// <summary>
        /// DO NOT USE: This method is for Unit Testing access. Use CalculateTax<T> instead
        /// </summary>
        public async Task<(TaxAmountResponseModel responseModel, string exceptionMessage)> CalculateTax(double amount, double shipping, string toCountry, string toZip, string toState, string fromState, Type type)
        {
            return await Calculate(amount, shipping, toCountry, toZip, toState, fromState, type);
        }

        private async Task<(TaxAmountResponseModel responseModel, string exceptionMessage)> Calculate(double amount, double shipping, string toCountry, string toZip, string toState, string fromState, Type type)
        {
            foreach (var calculator in _taxCalculators)
            {
                if (calculator.GetType() == type)
                {
                    return await calculator.CalcualateTax(amount, shipping, toCountry, toZip, toState, fromState);
                }
            }

            throw new TypeAccessException("The passed Type of ITaxCalculator does not exist in the collection");
        }
    }
}
