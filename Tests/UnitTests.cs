
using System.Threading.Tasks;
using FakeItEasy;
using NUnit.Framework;
using TaxJarAPI_POC.Infrastructure;
using TaxJarAPI_POC.Services;

namespace Tests
{
    public class UnitTests
    {
        private TaxJarTaxCalculator _taxCalculator;
        private TaxService _taxService;

        [SetUp]
        public void Setup()
        {
            _taxCalculator = A.Fake<TaxJarTaxCalculator>();
            _taxService = new TaxService(new[] { _taxCalculator });
        }

        [Test]
        public async Task TestNoExceptionMessage()
        {
            var (responseModel, exceptionMessage) = await _taxService.CalculateTax(100.00, 21.00, "US", "90210", "CA", "CA", _taxCalculator.GetType());

            Assert.AreEqual(string.Empty, exceptionMessage);
        }

        [Test]
        public async Task TestCalcualteTaxCorrectly()
        {
            var (responseModel, exceptionMessage) = await _taxService.CalculateTax(100.00, 21.00, "US", "90210", "CA", "CA", _taxCalculator.GetType());

            Assert.AreEqual(9.5, responseModel.Tax.AmountToCollect);
        }

        [Test]
        public async Task TestErrorMessageForMismatchedStateAndZipCode()
        {
            var (responseModel, exceptionMessage) = await _taxService.CalculateTax(100.00, 21.00, "US", "05832", "CA", "CA", _taxCalculator.GetType());

            Assert.AreEqual("to_zip 05832 is not used within to_state CA", exceptionMessage);
        }

        [Test]
        public async Task TestErrorMessageForMissingState()
        {
            var (responseModel, exceptionMessage) = await _taxService.CalculateTax(100.00, 21.00, "US", "90210", null, "CA", _taxCalculator.GetType());

            Assert.AreEqual("No to state/province, required when country is US", exceptionMessage);
        }
    }
}