﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TaxJarAPI_POC.Core.WebModels
{
    public partial class RateResponseModel
    {
        [JsonProperty("rate")]
        public Rate Rate { get; set; }
    }

    public partial class Rate
    {
        [JsonProperty("zip")]
        public long Zip { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("state_rate")]
        public string StateRate { get; set; }

        [JsonProperty("county")]
        public string County { get; set; }

        [JsonProperty("county_rate")]
        public string CountyRate { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("city_rate")]
        public string CityRate { get; set; }

        [JsonProperty("combined_district_rate")]
        public string CombinedDistrictRate { get; set; }

        [JsonProperty("combined_rate")]
        public string CombinedRate { get; set; }

        [JsonProperty("freight_taxable")]
        public bool FreightTaxable { get; set; }
    }
}
