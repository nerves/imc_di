﻿
using Xamarin.Forms;

namespace TaxJarAPI_POC.Core
{
    public partial class BasePage : ContentPage
    {
        private bool _viewHasLoaded;
        private bool _viewHasAppeared;
        public bool IgnoreActivity { get; set; }

        public BasePage()
        {
            InitializeComponent();
        }

        protected override SizeRequest OnSizeRequest(double widthConstraint, double heightConstraint)
        {
            return base.OnSizeRequest(widthConstraint, heightConstraint);
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            (BindingContext as BaseViewModel).Navigation = Navigation;
        }

        protected override void LayoutChildren(double x, double y, double width, double height)
        {
            base.LayoutChildren(x, y, width, height);
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (_viewHasLoaded == false)
            {
                _viewHasLoaded = true;
                (BindingContext as BaseViewModel).Prepare();
            }
        }

        protected override void OnAppearing()
        {
            // this allows us to navigate back from a push or model without reloading the view and subsequently, the view models
            if (_viewHasAppeared == true)
            {
                // optional refresh method for the view models to use
                (BindingContext as BaseViewModel).Refresh();
                return;
            }

            base.OnAppearing();
            (BindingContext as BaseViewModel).Start();

            _viewHasAppeared = true;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            (BindingContext as BaseViewModel).Stop();
        }



    }
}
