﻿
using System;
using System.Threading.Tasks;
using TaxJarAPI_POC.Core.WebModels;

namespace TaxJarAPI_POC.Core.Interfaces
{
    public interface ITaxService
    {
        public Task<(TaxAmountResponseModel responseModel, string exceptionMessage)> CalculateTax<T>(double amount, double shipping, string toCountry, string toZip, string toState, string fromState) where T : ITaxCalculator;
        public Task<(TaxAmountResponseModel responseModel, string exceptionMessage)> CalculateTax(double amount, double shipping, string toCountry, string toZip, string toState, string fromState, Type type);
    }
}
