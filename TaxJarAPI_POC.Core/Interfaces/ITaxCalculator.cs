﻿using System;
using System.Threading.Tasks;
using TaxJarAPI_POC.Core.WebModels;

namespace TaxJarAPI_POC.Core.Interfaces
{
    public interface ITaxCalculator
    {
        Task<(RateResponseModel model, string exception)> GetTaxRateForLocation(string zip);
        Task<(TaxAmountResponseModel model, string exception)> CalcualateTax(double amount, double shipping, string toCountry, string toZip, string toState, string fromState);
    }
}
