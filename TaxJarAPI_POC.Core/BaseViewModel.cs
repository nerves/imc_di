﻿
using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using PropertyChanged;
using TaxJarAPI_POC.Core.Interfaces;
using Xamarin.Forms;

namespace TaxJarAPI_POC.Core
{
    [AddINotifyPropertyChangedInterface]
    public abstract class BaseViewModel : IViewModel
    {
        protected BaseViewModel()
        {
            Guid = Guid.NewGuid();
        }

        public abstract Task Start();
        public abstract Task Stop();
        public abstract Task Refresh();
        public abstract Task Prepare();
        public abstract Task<bool> OnNavigatedBack();

        public Guid Guid { get; set; }
        public INavigation Navigation { get; set; }

        #region Busy View

        protected void ShowLoading(string message = null)
        {
            UserDialogs.Instance.ShowLoading(message);
        }

        protected void HideLoading()
        {
            UserDialogs.Instance.HideLoading();
        }

        #endregion
        #region Navigation

        public void NavigateToPagePush(Page page, bool animated = true, Action completion = null, Color navigationBarColor = default, Color navBarTextColor = default)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PushAsync(page, animated);
                completion?.Invoke();
            });
        }

        public void NavigateToPagePush(Type pageType, bool animated = true, Action completion = null, Color navigationBarColor = default, Color navBarTextColor = default)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            var page = (Page)Activator.CreateInstance(pageType);
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PushAsync(page);
                completion?.Invoke();
            });
        }

        public void NavigateToPageModal(Page page, bool animated = true, Action completion = null)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            var navView = new NavigationPage(page);
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PushModalAsync(navView, animated);
                completion?.Invoke();
            });
        }

        public void NavigateToPageModal(Type pageType, bool animated = true, Action completion = null)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            var navView = new NavigationPage((Page)Activator.CreateInstance(pageType));
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PushModalAsync(navView);
                completion?.Invoke();
            });
        }

        public void PopAsync(bool animated = true, Action completion = null)
        {
            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PopAsync(animated);
                completion?.Invoke();
            });
        }

        public void PopToRoot(bool animated = true, Action completion = null)
        {

            if (Navigation == null)
            {
                throw new NullReferenceException("This view model does not have a valid Navigation object reference.");
            }

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Navigation.PopToRootAsync(animated);
                completion?.Invoke();
            });
        }

        #endregion


        // NOTE: We use FodyWeavers.PropertyChanged to simplify the bindable
        // properties

        // i.e. public string Email { get; set; }

        // property changes can be observed like so

        // public void OnPropertyChanged(string propertyName, object before, object after) {}

        // OR

        // public void On<NameOfPropertyHere>Changed() {}

        // to enable, add [AddINotifyPropertyChangedInterface] above the View Model's Class name

    }
}
